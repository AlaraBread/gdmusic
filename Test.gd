extends Node

func set_instrument(i:int):
	var input_event:InputEventMIDI = InputEventMIDI.new()
	input_event.message = 0x0C # note on
	input_event.channel = 0x09
	input_event.instrument = i
	$MIDIPlayer.receive_raw_midi_message(input_event)

func _ready():
	$NoteEdit.value = note
	_on_BeatTimer_timeout()
	_on_SpeedEdit_value_changed($SpeedEdit.value)

onready var prev_note = note
func _on_BeatTimer_timeout():
	var input_event:InputEventMIDI = InputEventMIDI.new()
	input_event.message = 0x09 # note on
	input_event.pitch = note # note number
	input_event.channel = 0x09
	prev_note = note
	input_event.velocity = 0x7F # velocity
	$MIDIPlayer.receive_raw_midi_message(input_event)
	$NoteTimer.start()

func _on_NoteTimer_timeout():
	var input_event:InputEventMIDI = InputEventMIDI.new()
	input_event.message = 0x08 # note off
	input_event.channel = 0x09
	input_event.pitch = prev_note # note number
	input_event.velocity = 0x7F # velocity
	$MIDIPlayer.receive_raw_midi_message(input_event)

func _on_InstrumentEdit_value_changed(value):
	set_instrument(value)

var note = 30
func _on_NoteEdit_value_changed(value):
	note = value

func _on_SpeedEdit_value_changed(value):
	value = 60/value
	$BeatTimer.wait_time = value
	$NoteTimer.wait_time = value
