extends Node

export(int) var channel = 1
export(int) var instrument = 19
export(int) var velocity = 0xFF
export(int) var base_tones = 3
export(int) var extra_tones = 3
export(int) var note_offset = 0
var chords: = []

func _ready():
	MidiServer.set_instrument(channel, instrument)

func set_instrument(i:int):
	MidiServer.set_instrument(channel, i)

func generate_chords(scale, num):
	chords = []
	var prev = -1
	for c in range(num):
		var d = randi()%len(scale)
		if(c==0):
			d = 0
		while d == prev:
			d = randi()%len(scale)
		prev = d
		var chord = []
		for i in range(base_tones):
			chord.append(scale_deg(scale, d+(i*2)))
		for _i in range(extra_tones):
			chord.append(chord[randi()%base_tones]+12)
		chords.append(chord)

var cur_chord:int = 0
func reset():
	cur_chord = 0

func stop():
	#Turn the current chord off
	if(cur_chord >= len(chords)):
		return
	for note in chords[cur_chord]:
		MidiServer.note_off(channel, note+note_offset)

func play_next():
	#turn the previous chord off
	for note in chords[cur_chord]:
		MidiServer.note_off(channel, note+note_offset)
	#turn next chord on
	cur_chord = wrapi(cur_chord+1, 0, len(chords))
	var chord = chords[cur_chord]
	for note in chord:
		MidiServer.note_on(channel, note+note_offset, velocity)

func scale_deg(scale, d):
	var d_wrapped = wrapi(d, 0, len(scale))
	var out = scale[d_wrapped]+((d-d_wrapped)/len(scale))*12
	return out

func _on_ChordTimer_timeout():
	play_next()
